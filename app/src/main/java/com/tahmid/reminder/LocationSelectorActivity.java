package com.tahmid.reminder;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.GnssStatus;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.tahmid.reminder.adapter.LocationSelectionViewpagerAdapter;
import com.tahmid.reminder.fragment.FragmentModeSelect;
import com.tahmid.reminder.fragment.LocalityLanguageSelectionFragment;
import com.tahmid.reminder.fragment.LocationSelectorOptionsFragment;
import com.tahmid.reminder.widget.NonSwipableViewPager;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;

public class LocationSelectorActivity extends AppCompatActivity implements
        LocalityLanguageSelectionFragment.OnFragmentInteractionListener,
        FragmentModeSelect.OnFragmentInteractionListener, View.OnClickListener
        , GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    NonSwipableViewPager nsViewPager;
    LocationSelectionViewpagerAdapter locationSelectionViewpagerAdapter;
    TabLayout tabLayoutLocation;
    GoogleApiClient mGoogleApiClient = null;
    Location mCurrentLocation = null;
    String[] LOCATION_PERMISSIONS = new String[]{Manifest.permission.ACCESS_FINE_LOCATION};
    private String acquiredLocality="", acquiredCountry="";
    private Double acquiredLatitude;
    private Double acquiredLongitude;
    private static final int LOCATION_PERMISSION_REQUEST = 1;
    private static final int LOCATION_SETTINGS_REQUEST = 3;
    private static final int LANGUAGE_OPTIONS_PAGE = 0;
    private static final int LANGUAGE_LOCALITY_PAGE = 1;
    private static final int SELECT_MODE_PAGE = 2;
    private static final int LOCATION_WAIT_DURATION = 6000;
    LocationManager lm = null;
    ProviderListener listener = new ProviderListener();
    ProgressDialog locationLoadingPD;
    public PreferenceController preferenceController;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_selector);

        preferenceController = new PreferenceController(this);
        nsViewPager = (NonSwipableViewPager) findViewById(R.id.ns_viewpager_location_options);
        tabLayoutLocation = (TabLayout) findViewById(R.id.tab_layout_location);

        locationSelectionViewpagerAdapter = new LocationSelectionViewpagerAdapter(getSupportFragmentManager());
        lm = (LocationManager) getSystemService(LOCATION_SERVICE);
        locationLoadingPD = new ProgressDialog(this);


        nsViewPager.setAdapter(locationSelectionViewpagerAdapter);
        tabLayoutLocation.setupWithViewPager(nsViewPager);

        disableTabStrip();

        buildGoogleApiClient();

    }

    protected synchronized void buildGoogleApiClient() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    protected void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    public void disableTabStrip() {
        LinearLayout tabStrip = (LinearLayout) tabLayoutLocation.getChildAt(0);
        for (int i = 0; i < tabStrip.getChildCount(); i++) {
            tabStrip.getChildAt(i).setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return true;
                }
            });
        }
    }

    public void showLoadingProgress(){
        locationLoadingPD.setMessage(getString(R.string.loading_location_progress));
        locationLoadingPD.setCancelable(false);
        locationLoadingPD.show();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == android.R.id.text1) {
            if (((TextView) v).getText().toString().equals(getString(R.string.option_location_set_by_gps))) {
                   fetchLocation();

            } else if (((TextView) v).getText().toString().equals(getString(R.string.option_location_set_manually)))
                nsViewPager.setCurrentItem(LANGUAGE_LOCALITY_PAGE);
        }

    }

    protected synchronized void setLocalitybyGPS(){
            if(locationLoadingPD.isShowing())locationLoadingPD.dismiss();
            Geocoder geocoder = new Geocoder(this);
            List<Address> addressList = null;
            try {
                addressList = geocoder.getFromLocation(mCurrentLocation.getLatitude(),mCurrentLocation.getLongitude(),1);
                setAcquiredLocality(addressList.get(0).getLocality());
                setAcquiredCountry(addressList.get(0).getCountryName());
                setAcquiredLatitude(mCurrentLocation.getLatitude());
                setAcquiredLongitude(mCurrentLocation.getLongitude());
            } catch (IOException e) {
                e.printStackTrace();
            }
            nsViewPager.setCurrentItem(LANGUAGE_LOCALITY_PAGE);
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
    }

    public void showGpsOffDialog(){
        AlertDialog.Builder gpsDialogBuilder = new AlertDialog.Builder(this);
        gpsDialogBuilder.setTitle(getString(R.string.gps_on_dialog_title))
                        .setMessage(R.string.gps_on_dialog_message)
                        .setPositiveButton(getString(R.string.gps_on_dialog_positive_button), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent locationSettings = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivityForResult(locationSettings,LOCATION_SETTINGS_REQUEST);
                            }
                        })
                        .setCancelable(true)
                        .create().show();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    public void fetchLocation(){
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, LOCATION_PERMISSIONS, LOCATION_PERMISSION_REQUEST);
            return;
        } else{
            mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            lm.addGpsStatusListener(listener);

                if(mCurrentLocation!=null) {
                    setLocalitybyGPS();
                }else{
                    if(!lm.isProviderEnabled(LocationManager.GPS_PROVIDER))
                        showGpsOffDialog();
                    else{
                        new LocationLoadingTask().execute();
                    }

                }

        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case LOCATION_PERMISSION_REQUEST:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //location permission granted
                    fetchLocation();
                }else{
                    View parentView = findViewById(R.id.location_selector_parent_layout);
                    Snackbar.make(parentView,getString(R.string.location_permission_rationale), Snackbar.LENGTH_LONG).show();
                }
                break;
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == LOCATION_SETTINGS_REQUEST){
            if(!lm.isProviderEnabled(LocationManager.GPS_PROVIDER)){
                View parentView = findViewById(R.id.location_selector_parent_layout);
                Snackbar.make(parentView,getString(R.string.gps_turn_on_prompt),Snackbar.LENGTH_SHORT)
                        .show();
            }
        }
    }

    public String getAcquiredLocality() {
        return acquiredLocality;
    }

    public void setAcquiredLocality(String acquiredLocality) {
        this.acquiredLocality = acquiredLocality;
    }

    public String getAcquiredCountry() {
        return acquiredCountry;
    }

    public void setAcquiredCountry(String acquiredCountry) {
        this.acquiredCountry = acquiredCountry;
    }

    public Double getAcquiredLatitude(Double defValue) {
        if(acquiredLatitude == null) return defValue;
        else return acquiredLatitude;
    }

    public void setAcquiredLatitude(Double acquiredLatitude) {
        this.acquiredLatitude = acquiredLatitude;
    }

    public Double getAcquiredLongitude(double defValue) {
        if(acquiredLongitude==null) return  defValue;
        else return acquiredLongitude;
    }

    public void setAcquiredLongitude(Double acquiredLongitude) {
        this.acquiredLongitude = acquiredLongitude;
    }
    public void showGPSStatusChangeSnackbar(){
        View parentView = findViewById(R.id.location_selector_parent_layout);
            Snackbar.make(parentView,getString(R.string.gps_on_message),Snackbar.LENGTH_INDEFINITE)
                .setAction(getString(R.string.text_get_location), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        fetchLocation();
                    }
                })
                .setActionTextColor(getResources().getColor(android.R.color.holo_green_light))
                .show();
    }

    @Override
    public void onLocalityLanguageSelected() {
        nsViewPager.setCurrentItem(SELECT_MODE_PAGE);
    }

    public class LocationLoadingTask extends AsyncTask<Void,Location,Location>{

        long startTime,endTime;
        Location location=null;

        @Override
        protected void onProgressUpdate(Location... locations) {
            if(locations[0]!=null) this.cancel(true);
            super.onProgressUpdate(locations);
        }

        @Override
        protected Location doInBackground(Void... params) {

            startTime = System.currentTimeMillis();
            endTime = startTime + LOCATION_WAIT_DURATION;

            while(System.currentTimeMillis()<=endTime){
                try {
                    location=LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                    publishProgress(location);
                } catch (SecurityException e) {
                    e.printStackTrace();
                }
            }
            return location;
        }

        @Override
        protected void onPreExecute() {
            showLoadingProgress();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Location location) {
            mCurrentLocation = location;
            if(locationLoadingPD.isShowing())locationLoadingPD.dismiss();
            if(location!=null)
                fetchLocation();
            else
                Toast.makeText(LocationSelectorActivity.this,getString(R.string.location_error_fallback),Toast.LENGTH_SHORT).show();
            super.onPostExecute(location);
        }

        @Override
        protected void onCancelled(Location location) {
            mCurrentLocation = location;
            if(locationLoadingPD.isShowing())locationLoadingPD.dismiss();
            if(location!=null)
                fetchLocation();
            else
                Toast.makeText(LocationSelectorActivity.this,getString(R.string.location_error_fallback),Toast.LENGTH_SHORT).show();
            super.onCancelled(location);
        }
    }

    public class ProviderListener implements GpsStatus.Listener{

        @Override
        public void onGpsStatusChanged(int event) {

            if(event==GpsStatus.GPS_EVENT_STARTED){
                if(nsViewPager.getCurrentItem()==LANGUAGE_OPTIONS_PAGE)
                    showGPSStatusChangeSnackbar();
            }
        }
    }

    @TargetApi(24)
    public class ProviderListener_v24 extends GnssStatus.Callback{
        @Override
        public void onStarted() {
            showGPSStatusChangeSnackbar();
            super.onStarted();
        }
    }


}
