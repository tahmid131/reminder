package com.tahmid.reminder.parser;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;
import com.tahmid.reminder.Constants;
import com.tahmid.reminder.model.Language;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by TahmiD on 5/30/2017.
 */

public class CustomLangDeserializer implements JsonDeserializer<ArrayList<Language>> {
    @Override
    public ArrayList<Language> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonArray jsonArray = (JsonArray)json;

        JsonObject jsonObject = (JsonObject) jsonArray.get(jsonArray.size()-1);

        JsonArray jsonArrayLanguage = jsonObject.get(Constants.LANGUAGES).getAsJsonArray();

        Type listType = new TypeToken<ArrayList<Language>>(){}.getType();

        ArrayList<Language> languages = new Gson().fromJson(jsonArrayLanguage, listType);

        return languages;
    }
}
