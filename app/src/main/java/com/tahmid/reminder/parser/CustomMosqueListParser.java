package com.tahmid.reminder.parser;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;
import com.tahmid.reminder.Constants;
import com.tahmid.reminder.model.Mosque;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Created by TahmiD on 6/20/2017.
 */

public class CustomMosqueListParser implements JsonDeserializer<ArrayList<Mosque>> {

    @Override
    public ArrayList<Mosque> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

        ArrayList<Mosque> mosquesList = new ArrayList<Mosque>();
        JsonObject jsonObject = (JsonObject)json;
        String status = jsonObject.get(Constants.STATUS).getAsString();
        if(status.equals(Constants.STATUS_OK)){
            Type listType = new TypeToken<ArrayList<Mosque>>(){}.getType();
            mosquesList = new Gson().fromJson(jsonObject.getAsJsonArray(Constants.RESULTS), listType);
        }

        return mosquesList;

    }
}
