package com.tahmid.reminder.parser;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;
import com.tahmid.reminder.Constants;
import com.tahmid.reminder.model.SalatDetails;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by TahmiD on 2/25/2017.
 */
public class CustomDeserializer implements JsonDeserializer<SalatDetails> {
    @Override
    public SalatDetails deserialize(JsonElement json, Type type, JsonDeserializationContext context) throws JsonParseException {

        JsonObject jsonObject = (JsonObject)json;
        SalatDetails salatDetails = new SalatDetails();

        Gson gson = new Gson();

        salatDetails = gson.fromJson(jsonObject,SalatDetails.class);

        Type listType = new TypeToken<List<SalatDetails.Item>>(){}.getType();
        salatDetails.setItems((List<SalatDetails.Item>)
                gson.fromJson(jsonObject.getAsJsonArray(Constants.ITEMS),listType));

        return salatDetails;
    }
}
