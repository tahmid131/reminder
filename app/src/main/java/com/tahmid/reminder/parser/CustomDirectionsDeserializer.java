package com.tahmid.reminder.parser;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;
import com.tahmid.reminder.Constants;
import com.tahmid.reminder.model.MapPoint;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Created by TahmiD on 7/4/2017.
 */

public class CustomDirectionsDeserializer implements JsonDeserializer<ArrayList<MapPoint>> {
    @Override
    public ArrayList<MapPoint> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        ArrayList<MapPoint> mapPoints = new ArrayList<MapPoint>();
        JsonObject jsonObject = (JsonObject) json;
        String status = jsonObject.get(Constants.STATUS).getAsString();
        if(status.equals(Constants.STATUS_OK)){
            JsonArray routes = ((JsonObject) json).getAsJsonArray(Constants.ROUTES);
            JsonObject bestRoute = routes.get(0).getAsJsonObject();
            JsonArray legs = bestRoute.getAsJsonArray(Constants.LEGS);
            JsonObject legObjegt = legs.get(0).getAsJsonObject();
            Type listType = new TypeToken<ArrayList<MapPoint>>(){}.getType();
            mapPoints = new Gson().fromJson(legObjegt.getAsJsonArray(Constants.STEPS),listType);

        }
        return mapPoints;
    }
}

