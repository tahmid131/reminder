package com.tahmid.reminder;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by TahmiD on 2/7/2017.
 */
public  class AlarmReceiver extends WakefulBroadcastReceiver {

    public int NOTIFICATION_ID = 1 ;


    @Override
    public void onReceive(Context context, Intent intent) {

        Intent service = new Intent(context,ReminderWakefulService.class);
        startWakefulService(context,service);
    }

    public void wakeUpScreen(Context context){
        PowerManager pm = (PowerManager)context.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wakeLock = pm.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK|PowerManager.ACQUIRE_CAUSES_WAKEUP,"wakeup");
        wakeLock.acquire();
       // showNotification(context);
        wakeLock.release();
    }

    public void showNotification(Context context){
        int mId = NOTIFICATION_ID;
        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                                                    .setSmallIcon(R.mipmap.ic_launcher)
                                                    .setSound(uri)
                                                    .setContentTitle("Notification")
                                                    .setContentText("You set an alarm this time!")
                                                    .setAutoCancel(true);
        Intent resultIntent = new Intent(context,MainActivity.class);
        TaskStackBuilder taskStackBuilder = TaskStackBuilder.create(context);
        taskStackBuilder.addParentStack(MainActivity.class);
        taskStackBuilder.addNextIntent(resultIntent);

        PendingIntent pendingIntent = taskStackBuilder.getPendingIntent(0,PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(pendingIntent);
        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(mId,mBuilder.build());


    }
}
