package com.tahmid.reminder;

/**
 * In the name of Allah, Most Gracious, Most Merciful
 * Created by TahmiD on 2/11/2017.
 */
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Build;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParseException;
import com.tahmid.reminder.adapter.SalatWaqtListAdapter;
import com.tahmid.reminder.model.SalatDetails;
import com.tahmid.reminder.parser.CustomDeserializer;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements Response.Listener,Response.ErrorListener, View.OnClickListener{

    public String TAG = "ActivityTahmid";
    public AlarmManager alarmManager;
    public Intent intent;
    public PendingIntent alarmIntent;
    private RecyclerView mRecyclerViewSalatWaqtList;
    private LinearLayout llayoutFloatingButtonHolder;
    private SalatWaqtListAdapter salatWaqtListAdapter;
    private ProgressBar listLoadingProgressBar;
    private PreferenceController preferenceController;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        preferenceController = new PreferenceController(this);

        alarmManager = (AlarmManager)getSystemService(ALARM_SERVICE);
        intent = new Intent(this,AlarmReceiver.class);
        alarmIntent = PendingIntent.getBroadcast(this,0,intent,0);
        mRecyclerViewSalatWaqtList = (RecyclerView)findViewById(R.id.recyclerview_salat_time_list);
        llayoutFloatingButtonHolder = (LinearLayout)findViewById(R.id.llayout_floating_button_holder);
        listLoadingProgressBar = (ProgressBar)findViewById(R.id.progress_bar_list_loading);

        Animation slideButtons = AnimationUtils.loadAnimation(this,R.anim.fade_in_move_left_500);
        llayoutFloatingButtonHolder.startAnimation(slideButtons);

        ((FloatingActionButton)findViewById(R.id.fab_direction)).setOnClickListener(this);
        ((FloatingActionButton)findViewById(R.id.fab_settings)).setOnClickListener(this);


        getSalatDetails();

    }

    public void getSalatDetails(){
        listLoadingProgressBar.setVisibility(View.VISIBLE);
        String url = "http://muslimsalat.com/dhaka.json";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        MySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);
    }


    public Calendar parseTime(String time){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm aa", Locale.getDefault());
        Calendar calendar = Calendar.getInstance();
        try {
            Date date = simpleDateFormat.parse(time);

            //saving current date
            int day = calendar.get(Calendar.DAY_OF_MONTH);
            int month = calendar.get(Calendar.MONTH);
            int year = calendar.get(Calendar.YEAR);
            //saving time from string but that replaces current date by initial 1-1-1970
            calendar.setTime(date);
            //restoring current date
            calendar.set(Calendar.DAY_OF_MONTH,day);
            calendar.set(Calendar.MONTH,month);
            calendar.set(Calendar.YEAR,year);
            return calendar;

        } catch (ParseException e) {
            Toast.makeText(MainActivity.this,"Invalid time format!!",Toast.LENGTH_LONG).show();
            e.printStackTrace();
            return calendar;
        }
    }


    public void setAlarm(String time){
        Calendar c = parseTime(time);
        alarmManager.cancel(alarmIntent);
        if(Build.VERSION.SDK_INT<Build.VERSION_CODES.KITKAT){
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,c.getTimeInMillis(),AlarmManager.INTERVAL_DAY,alarmIntent);
        }else{
            alarmManager.setExact(AlarmManager.RTC_WAKEUP,c.getTimeInMillis(),alarmIntent);
        }
    }


    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onResponse(Object response) {

        try {
            listLoadingProgressBar.setVisibility(View.GONE);
            Gson gson = new GsonBuilder().registerTypeAdapter(SalatDetails.class, new CustomDeserializer()).create();
            SalatDetails salatDetails = gson.fromJson(response.toString(),SalatDetails.class);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
            salatWaqtListAdapter = new SalatWaqtListAdapter(salatDetails.getItems().get(0));
            mRecyclerViewSalatWaqtList.setLayoutManager(mLayoutManager);
            mRecyclerViewSalatWaqtList.setItemAnimator(new DefaultItemAnimator());
            mRecyclerViewSalatWaqtList.setAdapter(salatWaqtListAdapter);

        } catch (JsonParseException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.fab_direction:
                startActivity(new Intent(MainActivity.this,DirectionsActivity.class));
                break;
            case R.id.fab_settings:
                startActivity(new Intent(MainActivity.this,LocationSelectorActivity.class));
                break;
            default:
                break;
        }
    }
}
