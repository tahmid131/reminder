package com.tahmid.reminder;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class SplashActivity extends AppCompatActivity {

    private static int SPLASH_ACTIVITY_DURATION = 3000;
    private PreferenceController preferenceController;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        preferenceController = new PreferenceController(this);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent appMain = null;
                if(preferenceController.getAppFirstRun())
                    appMain = new Intent(SplashActivity.this,LocationSelectorActivity.class);
                else
                    appMain = new Intent(SplashActivity.this,MainActivity.class);

                startActivity(appMain);
                finish();
            }
        },SPLASH_ACTIVITY_DURATION);
    }
}
