package com.tahmid.reminder.fragment;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatSpinner;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.tahmid.reminder.LocationSelectorActivity;
import com.tahmid.reminder.MySingleton;
import com.tahmid.reminder.PreferenceController;
import com.tahmid.reminder.R;
import com.tahmid.reminder.adapter.LangSpinnerAdapter;
import com.tahmid.reminder.model.Language;
import com.tahmid.reminder.parser.CustomLangDeserializer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.lang.reflect.Type;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link LocalityLanguageSelectionFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link LocalityLanguageSelectionFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LocalityLanguageSelectionFragment extends Fragment implements
        Response.Listener,Response.ErrorListener, View.OnClickListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    String currentCountry ;
    private ProgressBar languageLoadingProgressBar;
    private TextView textViewNetworkError;
    private int languageEnglishPosition ;
    private Gson gson;
    private Type listType;
    private Double currentLatitude,currentLongitude;
    private AppCompatSpinner spinnerLanguage;
    private LangSpinnerAdapter spinnerAdapter;
    private ArrayList<Language> appSupportedLanguages;
    private EditText autoCompleteEditText;
    private PreferenceController preferenceController;

    public LocalityLanguageSelectionFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment LocalityLanguageSelectionFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static LocalityLanguageSelectionFragment newInstance(String param1, String param2) {
        LocalityLanguageSelectionFragment fragment = new LocalityLanguageSelectionFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);

        }
        listType = new TypeToken<ArrayList<Language>>(){}.getType();
        gson = new GsonBuilder().registerTypeAdapter(listType, new CustomLangDeserializer()).create();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View fragmentView = inflater.inflate(R.layout.fragment_locality_language_selection, container, false);
        languageLoadingProgressBar = (ProgressBar) fragmentView.findViewById(R.id.progress_bar_loading_language);
        textViewNetworkError = (TextView) fragmentView.findViewById(R.id.textview_network_error);
        textViewNetworkError.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!currentCountry.isEmpty())getLocalebyCountry(currentCountry.substring(currentCountry.lastIndexOf(" ")+1));
            }
        });

        spinnerLanguage = (AppCompatSpinner) fragmentView.findViewById(R.id.spinner_language);
        appSupportedLanguages = gson.fromJson(getJsonById(R.raw.app_supported_locales),listType);

        for(Language language:appSupportedLanguages){
            if(language.getIso639_1().equals("en"))
                languageEnglishPosition = appSupportedLanguages.indexOf(language);
        }

        spinnerAdapter = new LangSpinnerAdapter(getContext(),android.R.layout.simple_spinner_item,
                            android.R.id.text1,appSupportedLanguages);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerLanguage.setAdapter(spinnerAdapter);

        ((AppCompatButton)fragmentView.findViewById(R.id.button_select_locale)).setOnClickListener(this);

        languageLoadingProgressBar.setVisibility(View.GONE);
        textViewNetworkError.setVisibility(View.GONE);

        return fragmentView;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        preferenceController = new PreferenceController(getActivity());
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if(isVisibleToUser){
            initPlacesACLocality();
            currentCountry = ((LocationSelectorActivity)getActivity()).getAcquiredCountry();
            currentLatitude=((LocationSelectorActivity)getActivity()).getAcquiredLatitude(0.0);
            currentLongitude=((LocationSelectorActivity)getActivity()).getAcquiredLongitude(0.0);
            if(!currentCountry.isEmpty())getLocalebyCountry(currentCountry.substring(currentCountry.lastIndexOf(" ")+1));
        }
    }

    public void getLocalebyCountry(String country){
        try {
            textViewNetworkError.setVisibility(View.GONE);
            languageLoadingProgressBar.setVisibility(View.VISIBLE);
            String url = "https://restcountries.eu/rest/v2/name/"+ URLEncoder.encode(country,"UTF-8");
            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET,url,null,this,this);
            MySingleton.getInstance(getActivity()).addToRequestQueue(jsonArrayRequest);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public String getJsonById(int id){
        InputStream is = getResources().openRawResource(id);
        Writer writer = new StringWriter();
        char[] buffer = new char[1024];
        try {
            Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            int n;
            while ((n = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, n);
            }
            is.close();
        }catch ( IOException e) {
            e.printStackTrace();
        }


        String jsonString = writer.toString();
        return jsonString ;
    }

    public void initPlacesACLocality(){
        PlaceAutocompleteFragment autocompleteFragment = (PlaceAutocompleteFragment)
                getActivity().getFragmentManager().findFragmentById(R.id.places_ac_locality);

        AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                .setTypeFilter(AutocompleteFilter.TYPE_FILTER_CITIES)
                .build();

        autoCompleteEditText = (EditText) autocompleteFragment.getView().findViewById(com.google.android.gms.R.id.place_autocomplete_search_input);
        autoCompleteEditText.setText(((LocationSelectorActivity)getActivity()).getAcquiredLocality());
        autocompleteFragment.setFilter(typeFilter);
        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // TODO: Get info about the selected place.
                // Log.i(TAG, "Place: " + place.getName());
                Geocoder geocoder = new Geocoder(getActivity());
                List<Address> addressList = null;
                try {
                    addressList = geocoder.getFromLocation(place.getLatLng().latitude,place.getLatLng().longitude,1);
                    currentCountry = addressList.get(0).getCountryName();
                    getLocalebyCountry(currentCountry.substring(currentCountry.lastIndexOf(" ")+1));
                    currentLatitude=place.getLatLng().latitude;
                    currentLongitude =place.getLatLng().longitude;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                // Log.i(TAG, "An error occurred: " + status);
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        languageLoadingProgressBar.setVisibility(View.GONE);
        textViewNetworkError.setVisibility(View.VISIBLE);
    }

    @Override
    public void onResponse(Object response) {
        languageLoadingProgressBar.setVisibility(View.GONE);
        ArrayList<Language> languagesFromApi = gson.fromJson(response.toString(),listType);
        setLanguageSelected(languagesFromApi);
    }

    public void setLanguageSelected(ArrayList<Language> languagesFromApi){


        int selectedLanguagePosition = languageEnglishPosition;
        for(Language language:languagesFromApi){
            for(Language appSupported:appSupportedLanguages){
                if((language.getIso639_1().equals(appSupported.getIso639_1()))
                        && (appSupportedLanguages.indexOf(appSupported)!=languageEnglishPosition))
                    selectedLanguagePosition = appSupportedLanguages.indexOf(appSupported);
            }

        }
        spinnerLanguage.setSelection(selectedLanguagePosition);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button_select_locale:
                if (mListener != null) {
                    if(!autoCompleteEditText.getText().toString().isEmpty()){
                        preferenceController.putUserLocality(autoCompleteEditText.getText().toString());
                        preferenceController.putUserLanguage(((Language)spinnerLanguage.getSelectedItem()).getIso639_1());
                        preferenceController.putUserLatitude(currentLatitude);
                        preferenceController.putUserLongitude(currentLongitude);
                        mListener.onLocalityLanguageSelected();
                    } else{
                        Toast.makeText(getActivity(),"Please select your locality",Toast.LENGTH_LONG).show();
                    }
                }
                break;
            default:
                break;
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onLocalityLanguageSelected();
    }
}
