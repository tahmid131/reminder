package com.tahmid.reminder.model;


import java.util.List;

/**
 * Created by TahmiD on 2/25/2017.
 */
public class SalatDetails {
    private int status_valid;
    private int status_code;
    private String status_description;
    private List<Item> items;
    private String link;
    private String qibla_direction;
    private String latitude;
    private String longitude;
    private String address;
    private String city;
    private String state;
    private String postal_code;
    private String country;
    private String country_code;


    public int getStatus_valid() {
        return status_valid;
    }

    public void setStatus_valid(int status_valid) {
        this.status_valid = status_valid;
    }

    public int getStatus_code() {
        return status_code;
    }

    public void setStatus_code(int status_code) {
        this.status_code = status_code;
    }

    public String getStatus_description() {
        return status_description;
    }

    public void setStatus_description(String status_description) {
        this.status_description = status_description;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getQibla_direction() {
        return qibla_direction;
    }

    public void setQibla_direction(String qibla_direction) {
        this.qibla_direction = qibla_direction;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPostal_code() {
        return postal_code;
    }

    public void setPostal_code(String postal_code) {
        this.postal_code = postal_code;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountry_code() {
        return country_code;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }


    public class Item{

        private String date_for;
        private String fajr;
        private String shurooq;
        private String dhuhr;
        private String asr;
        private String maghrib;
        private String isha;

        public String getDate_for() {
            return date_for;
        }

        public void setDate_for(String date_for) {
            this.date_for = date_for;
        }

        public String getFajr() {
            return fajr;
        }

        public void setFajr(String fajr) {
            this.fajr = fajr;
        }

        public String getShurooq() {
            return shurooq;
        }

        public void setShurooq(String shurooq) {
            this.shurooq = shurooq;
        }

        public String getDhuhr() {
            return dhuhr;
        }

        public void setDhuhr(String dhuhr) {
            this.dhuhr = dhuhr;
        }

        public String getAsr() {
            return asr;
        }

        public void setAsr(String asr) {
            this.asr = asr;
        }

        public String getMaghrib() {
            return maghrib;
        }

        public void setMaghrib(String maghrib) {
            this.maghrib = maghrib;
        }

        public String getIsha() {
            return isha;
        }

        public void setIsha(String isha) {
            this.isha = isha;
        }




    }

}
