package com.tahmid.reminder.model;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by TahmiD on 7/4/2017.
 */

public class MapPoint {

    public PolylineCustom polyline;


    public class PolylineCustom{
        private String points;

        public String getPoints() {
            return points;
        }

        public void setPoints(String points) {
            this.points = points;
        }
    }
}
