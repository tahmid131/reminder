package com.tahmid.reminder;

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.EditText;

/**
 * Created by TahmiD on 6/7/2017.
 */

public class PreferenceController {
    public static final String appPrefsName = "ReminderPrefs";

    private static final String appFirstRun = "isFirstRun";
    private static final String userLocality = "locality";
    private static final String userAddressLine = "address";
    private static final String userLanguage = "lang_iso639_1";
    private static final String userLatitude = "latitude";
    private static final String userLongitude = "longitude";

    private static final String userModeIsNormal = "isNormal";


    SharedPreferences appPreference;

    public PreferenceController(Context context) {
        appPreference = context.getSharedPreferences(appPrefsName,Context.MODE_PRIVATE);
    }

    public boolean getUserModeIsNormal(){
        return appPreference.getBoolean(userModeIsNormal,false);
    }

    public void putUserModeIsNormal(boolean value){
        SharedPreferences.Editor editor = appPreference.edit();
        editor.putBoolean(userModeIsNormal,value);
        editor.apply();
    }

    public boolean getAppFirstRun(){
        return appPreference.getBoolean(appFirstRun,true);
    }

    public void putAppFirstRun(boolean value){
        SharedPreferences.Editor editor = appPreference.edit();
        editor.putBoolean(appFirstRun,value);
        editor.apply();
    }

    public void putUserLocality(String locality){
        SharedPreferences.Editor editor = appPreference.edit();
        editor.putString(userLocality,locality);
        editor.apply();
    }

    public String getUserLocality(){
        return appPreference.getString(userLocality,"");
    }

    public void putUserAddressLine(String addressLine){
        SharedPreferences.Editor editor = appPreference.edit();
        editor.putString(userAddressLine,addressLine);
        editor.apply();
    }

    public String getUserAddressLine(){
        return appPreference.getString(userAddressLine,"");
    }

    public void putUserLanguage(String language){
        SharedPreferences.Editor editor = appPreference.edit();
        editor.putString(userLanguage,language);
        editor.apply();
    }

    public String getUserLanguage(){
        return appPreference.getString(userAddressLine,"en");
    }

    public void putUserLatitude(double latitude){
        SharedPreferences.Editor editor = appPreference.edit();
        editor.putLong(userLatitude,Double.doubleToRawLongBits(latitude));
        editor.apply();
    }

    public Double getUsetLatitude(){
        if(!appPreference.contains(userLatitude)) return Double.valueOf(0);
        else return Double.longBitsToDouble(appPreference.getLong(userLatitude,0));
    }


    public void putUserLongitude(double longitude){
        SharedPreferences.Editor editor = appPreference.edit();
        editor.putLong(userLongitude,Double.doubleToRawLongBits(longitude));
        editor.apply();
    }

    public Double getUsetLongitude(){
        if(!appPreference.contains(userLongitude)) return Double.valueOf(0);
        else return Double.longBitsToDouble(appPreference.getLong(userLongitude,0));
    }
}
