package com.tahmid.reminder;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

/**
 * Created by TahmiD on 2/11/2017.
 */
public class ReminderWakefulService extends IntentService {

    public int NOTIFICATION_ID = 1 ;

    public ReminderWakefulService() {
        super("ReminderWakefulService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        //Wakes up the screen
        PowerManager powerManager = (PowerManager)getSystemService(POWER_SERVICE);
        PowerManager.WakeLock wakeLock = powerManager.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK|PowerManager.ACQUIRE_CAUSES_WAKEUP,"wakelock");
        wakeLock.acquire();
        showNotification(getApplicationContext());
        wakeLock.release();
        AlarmReceiver.completeWakefulIntent(intent);
    }

    public void showNotification(Context context){
        int mId = NOTIFICATION_ID;
        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setSound(uri)
                .setContentTitle("Notification")
                .setContentText("You set an alarm this time!")
                .setAutoCancel(true);
        Intent resultIntent = new Intent(context,MainActivity.class);
        TaskStackBuilder taskStackBuilder = TaskStackBuilder.create(context);
        taskStackBuilder.addParentStack(MainActivity.class);
        taskStackBuilder.addNextIntent(resultIntent);

        PendingIntent pendingIntent = taskStackBuilder.getPendingIntent(0,PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(pendingIntent);
        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(mId,mBuilder.build());


    }

}
