package com.tahmid.reminder;

/**
 * Created by TahmiD on 3/12/2017.
 */
public class Constants {

    public static final int NUMBER_SALAT_WAQT = 5;
    //response_status
    public static final String STATUS = "status";
    public static final String STATUS_OK = "OK";
    public static final String STATUS_VALID = "status_valid";
    public static final String STATUS_CODE = "status_code";
    public static final String STATUS_DESCRIPTION = "status_description";
    public static final String RESULTS = "results";
    public static final String ROUTES = "routes";
    public static final String STEPS = "steps";
    public static final String LEGS = "legs";

    //contents
    public static final String ITEMS = "items";
    public static final String LINK = "link";
    public static final String QIBLA_DIRECTION = "qibla_direction";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";
    public static final String ADDRESS = "address";
    public static final String CITY = "city";
    public static final String STATE = "state";
    public static final String POSTAL_CODE = "postal_code";
    public static final String COUNTRY = "country";
    public static final String COUNTRY_CODE = "country_code";

    //rest-countries-language
    public static final String LANGUAGES = "languages";
}
