package com.tahmid.reminder.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tahmid.reminder.Constants;
import com.tahmid.reminder.R;
import com.tahmid.reminder.model.SalatDetails;


/**
 * Created by TahmiD on 3/13/2017.
 */
public class SalatWaqtListAdapter extends RecyclerView.Adapter<SalatWaqtListAdapter.WaqtViewHolder> {
    SalatDetails.Item item;
    Context context;


    public SalatWaqtListAdapter(SalatDetails.Item item) {
        this.item = item;
    }

    @Override
    public WaqtViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.context = parent.getContext();
        View listItemView = LayoutInflater.from(context).inflate(R.layout.layout_salat_list_single_item,parent,false);
        return new WaqtViewHolder(listItemView);
    }

    @Override
    public void onBindViewHolder(WaqtViewHolder holder, int position) {

        switch (position){
            case 0:
                holder.textViewSalatName.setText(context.getString(R.string.text_waqt_fajr));
                holder.textViewSalatWaqt.setText(item.getFajr());
                break;
            case 1:
                holder.textViewSalatName.setText(context.getString(R.string.text_waqt_dhuhr));
                holder.textViewSalatWaqt.setText(item.getDhuhr());
                break;
            case 2:
                holder.textViewSalatName.setText(context.getString(R.string.text_waqt_asr));
                holder.textViewSalatWaqt.setText(item.getAsr());
                break;
            case 3:
                holder.textViewSalatName.setText(context.getString(R.string.text_waqt_maghrib));
                holder.textViewSalatWaqt.setText(item.getMaghrib());
                break;
            case 4:
                holder.textViewSalatName.setText(context.getString(R.string.text_waqt_isha));
                holder.textViewSalatWaqt.setText(item.getIsha());
                break;
        }
    }


    @Override
    public int getItemCount() {
        return Constants.NUMBER_SALAT_WAQT;
    }

    public class WaqtViewHolder extends RecyclerView.ViewHolder{

        TextView textViewSalatName, textViewSalatWaqt;
        public WaqtViewHolder(View itemView) {
            super(itemView);
            textViewSalatName = (TextView)itemView.findViewById(R.id.textview_salat_waqt_name);
            textViewSalatWaqt = (TextView)itemView.findViewById(R.id.textview_salat_waqt);
        }
    }
}
