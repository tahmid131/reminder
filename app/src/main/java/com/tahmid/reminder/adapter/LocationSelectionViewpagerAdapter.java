package com.tahmid.reminder.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.tahmid.reminder.fragment.FragmentModeSelect;
import com.tahmid.reminder.fragment.LocalityLanguageSelectionFragment;
import com.tahmid.reminder.fragment.LocationSelectorOptionsFragment;


/**
 * Created by TahmiD on 3/19/2017.
 */
public class LocationSelectionViewpagerAdapter extends FragmentStatePagerAdapter{

    static final int NUM_FRAGMENTS = 3;

    public LocationSelectionViewpagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        if(position==0){
            return new LocationSelectorOptionsFragment();
        }else if(position==1)
            return new LocalityLanguageSelectionFragment();
        else
            return new FragmentModeSelect();
    }

    @Override
    public int getCount() {
        return NUM_FRAGMENTS;
    }
}
