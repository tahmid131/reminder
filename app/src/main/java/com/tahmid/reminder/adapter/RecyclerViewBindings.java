package com.tahmid.reminder.adapter;


import android.content.res.TypedArray;
import android.databinding.BindingAdapter;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tahmid.reminder.LocationSelectorActivity;
import com.tahmid.reminder.R;

/**
 * Created by TahmiD on 3/18/2017.
 */
public class RecyclerViewBindings {

    @BindingAdapter("entries")
    public static void entries(RecyclerView recyclerView, String[] strings){
        recyclerView.setAdapter(new SimpleArrayAdapter(strings));
    }

    static class SimpleArrayAdapter extends RecyclerView.Adapter<SimpleHolder>{

        private final String[] mArray;

        public SimpleArrayAdapter(String[] strings){
            mArray = strings;
        }


        @Override
        public SimpleHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
            final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            final View view = inflater.inflate(android.R.layout.simple_list_item_1,parent,false);
            view.setClickable(true);

            int[] attrs = {R.attr.selectableItemBackground};
            TypedArray ta = parent.getContext().obtainStyledAttributes(attrs);
            view.setBackground(ta.getDrawable(0));
            ta.recycle();
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((LocationSelectorActivity)parent.getContext()).onClick(v);
                }
            });
            return new SimpleHolder(view);
        }

        @Override
        public void onBindViewHolder(SimpleHolder holder, int position) {
            holder.mTextView.setText(mArray[position]);
        }

        @Override
        public int getItemCount() {
            return mArray.length;
        }

    }

    static class SimpleHolder extends RecyclerView.ViewHolder{

        private final TextView mTextView;

        public SimpleHolder(View itemView) {
            super(itemView);
            mTextView = (TextView)itemView.findViewById(android.R.id.text1);
        }
    }
}
