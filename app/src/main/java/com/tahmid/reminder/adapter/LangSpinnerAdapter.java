package com.tahmid.reminder.adapter;

import android.content.Context;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.tahmid.reminder.model.Language;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by TahmiD on 6/4/2017.
 */

public class LangSpinnerAdapter extends ArrayAdapter {


    public LangSpinnerAdapter(@NonNull Context context, @LayoutRes int resource, @IdRes int textViewResourceId, @NonNull List objects) {
        super(context, resource, textViewResourceId, objects);
}

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Language language = (Language) getItem(position);
        if(convertView==null){
            convertView = super.getView(position, convertView, parent);
        }
        TextView textView = (TextView) convertView.findViewById(android.R.id.text1);
        textView.setText(language.getNativeName());

        return convertView;
    }


    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Language language = (Language) getItem(position);
        if(convertView==null){
            convertView = super.getDropDownView(position, convertView, parent);
        }
        TextView textView = (TextView) convertView.findViewById(android.R.id.text1);
        textView.setText(language.getNativeName());

        return convertView;
    }
}
