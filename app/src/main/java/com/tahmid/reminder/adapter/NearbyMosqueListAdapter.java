package com.tahmid.reminder.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tahmid.reminder.DirectionsActivity;
import com.tahmid.reminder.R;
import com.tahmid.reminder.model.Mosque;

import java.util.ArrayList;


/**
 * Created by TahmiD on 6/20/2017.
 */

public class NearbyMosqueListAdapter extends RecyclerView.Adapter<NearbyMosqueListAdapter.MosqueListViewHolder> {

    ArrayList<Mosque> mosques;
    Context context;

    public NearbyMosqueListAdapter(ArrayList<Mosque> mosques) {
        this.mosques = mosques;
    }

    @Override
    public MosqueListViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        this.context = parent.getContext();
        View itemView = LayoutInflater.from(context).inflate(R.layout.mosque_list_single_item,parent,false);

        return new MosqueListViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(NearbyMosqueListAdapter.MosqueListViewHolder holder, final int position) {
        holder.textViewMosqueName.setText(mosques.get(position).getName());
        holder.textViewMosqueVicinity.setText(mosques.get(position).getVicinity());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((DirectionsActivity)context).setMosqueSelectedUIVisible(mosques.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return mosques.size();
    }

    public void updateData(ArrayList<Mosque> mosques){
        this.mosques = mosques;
    }

    public class MosqueListViewHolder extends RecyclerView.ViewHolder{

        TextView textViewMosqueName;
        TextView textViewMosqueVicinity;

        public MosqueListViewHolder(View itemView) {
            super(itemView);
            textViewMosqueName = (TextView) itemView.findViewById(R.id.textview_mosque_name);
            textViewMosqueVicinity = (TextView) itemView.findViewById(R.id.textview_mosque_vicinity);
        }
    }
}
