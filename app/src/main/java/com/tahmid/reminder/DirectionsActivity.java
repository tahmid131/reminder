package com.tahmid.reminder;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.google.maps.android.PolyUtil;
import com.tahmid.reminder.adapter.NearbyMosqueListAdapter;
import com.tahmid.reminder.model.MapPoint;
import com.tahmid.reminder.model.Mosque;
import com.tahmid.reminder.parser.CustomDirectionsDeserializer;
import com.tahmid.reminder.parser.CustomMosqueListParser;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class DirectionsActivity extends AppCompatActivity implements View.OnClickListener,
        OnMapReadyCallback{

    Toolbar toolbar;
    PreferenceController preferenceController;
    ArrayList<Mosque> mosques;
    ArrayList<MapPoint> mapPoints;
    RecyclerView nearbyMosqueRecyclerView;
    NearbyMosqueListAdapter adapter;
    SupportMapFragment mapFragment;
    private GoogleMap mMap;
    private LatLng selectedMosqueLatLng;
    PolylineOptions polylineOptions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_directions);
        toolbar = (Toolbar)findViewById(R.id.toolbar_directions);
        toolbar.setTitle(getString(R.string.title_directions));

        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        preferenceController = new PreferenceController(this);

        nearbyMosqueRecyclerView = (RecyclerView) findViewById(R.id.recyclerview_nearby_mosques_list);
        mosques = new ArrayList<Mosque>();
        mapPoints = new ArrayList<MapPoint>();
        polylineOptions = new PolylineOptions();
        adapter = new NearbyMosqueListAdapter(mosques);
        nearbyMosqueRecyclerView.setAdapter(adapter);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        nearbyMosqueRecyclerView.setLayoutManager(mLayoutManager);

        mapFragment = (SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.map);
        ((ImageButton)findViewById(R.id.imagebutton_close_details)).setOnClickListener(this);
        getNearbyMosques();
    }

    public void getNearbyMosques(){

        String url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location="+String.valueOf(preferenceController.getUsetLatitude())+","+
                String.valueOf(preferenceController.getUsetLongitude())+"&radius=500&type=mosque&key="
                +getResources().getString(R.string.google_places_web_services_key);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d("Directions",response.toString());
                Type listType = new  TypeToken<ArrayList<Mosque>>(){}.getType();
                Gson gson = new GsonBuilder().registerTypeAdapter(listType,new CustomMosqueListParser()).create();
                mosques = gson.fromJson(response.toString(),listType);
                adapter.updateData(mosques);
                adapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Directions",error.toString());
            }
        });
        MySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);
    }

    public void getMosqueDirections(Double latitude,Double longitude){
        String url = "https://maps.googleapis.com/maps/api/directions/json?origin="+preferenceController.getUsetLatitude()+","+
                preferenceController.getUsetLongitude()+"&destination="+latitude+","+longitude+"&key="+getResources().getString(R.string.google_places_web_services_key);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url,null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {

                Type listType = new  TypeToken<ArrayList<MapPoint>>(){}.getType();
                Gson gson = new GsonBuilder().registerTypeAdapter(listType,new CustomDirectionsDeserializer()).create();
                mapPoints = gson.fromJson(response.toString(),listType);

                if(!polylineOptions.getPoints().isEmpty())
                    polylineOptions.getPoints().clear();
                for(MapPoint point: mapPoints){
                    polylineOptions.addAll(PolyUtil.decode(point.polyline.getPoints()));
                }

                if(mMap!=null)mMap.clear();
                mapFragment.getMapAsync(DirectionsActivity.this);


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Directions",error.toString());
            }
        });

        MySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imagebutton_close_details:
                setMosqueSelectedUIInvisible();
                break;
        }
    }

    public void setMosqueSelectedUIVisible(Mosque mosque){
        Animation fadeIn = AnimationUtils.loadAnimation(this,R.anim.fade_in);
        Animation fadeInMoveLeft = AnimationUtils.loadAnimation(this,R.anim.fade_in_move_left_500);


        fadeIn.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                ((LinearLayout)findViewById(R.id.layout_mosque_headline)).setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        fadeInMoveLeft.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                ((FrameLayout)findViewById(R.id.framelayout_map)).setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        ((TextView)findViewById(R.id.textview_headline_mosque_name)).setText(mosque.getName());
        ((LinearLayout)findViewById(R.id.layout_mosque_headline)).startAnimation(fadeIn);
        ((FrameLayout)findViewById(R.id.framelayout_map)).startAnimation(fadeInMoveLeft);
        selectedMosqueLatLng = new LatLng(mosque.getGeometry().getLocation().getLat(),
                                        mosque.getGeometry().getLocation().getLng());

        getMosqueDirections(mosque.getGeometry().getLocation().getLat(),
                mosque.getGeometry().getLocation().getLng());

    }

    public void setMosqueSelectedUIInvisible(){
        Animation fadeOut = AnimationUtils.loadAnimation(this,R.anim.fade_out);
        Animation fadeOutMoveLeft = AnimationUtils.loadAnimation(this,R.anim.fade_out_move_left_500);


        fadeOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                ((LinearLayout)findViewById(R.id.layout_mosque_headline)).setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        fadeOutMoveLeft.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                ((FrameLayout)findViewById(R.id.framelayout_map)).setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        ((LinearLayout)findViewById(R.id.layout_mosque_headline)).startAnimation(fadeOut);
        ((FrameLayout)findViewById(R.id.framelayout_map)).startAnimation(fadeOutMoveLeft);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        try {
            mMap = googleMap;
            LatLng userLocation = new LatLng(preferenceController.getUsetLatitude(),preferenceController.getUsetLongitude());
            mMap.addMarker(new MarkerOptions().position(userLocation).title("Your Location"));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(userLocation));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(userLocation,16));
            mMap.addPolyline(polylineOptions.geodesic(true)).setColor(getResources().getColor(R.color.colorAccent));

        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

}
