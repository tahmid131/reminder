package com.tahmid.reminder.widget;

import android.content.Context;
import android.icu.text.DateFormat;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.animation.DecelerateInterpolator;
import android.widget.Scroller;

import java.lang.reflect.Field;

/**
 * Created by TahmiD on 3/19/2017.
 */
public class NonSwipableViewPager extends ViewPager {

    public NonSwipableViewPager(Context context){
        super(context);
        setCustomScroller();
    }

    public NonSwipableViewPager(Context context, AttributeSet attrs){
        super(context,attrs);
        setCustomScroller();
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        //to prevent swipe between fragments
        return false;
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        //to prevent swipe between fragments
        return false;
    }

    private void setCustomScroller(){
        try {
            Class<?> viewPager = ViewPager.class;
            Field scroller = viewPager.getDeclaredField("mScroller");
            scroller.setAccessible(true);
            scroller.set(this,new CustomScroller(getContext()));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public class CustomScroller extends Scroller{

        public CustomScroller(Context context){
            super(context,new DecelerateInterpolator());
        }

        @Override
        public void startScroll(int startX, int startY, int dx, int dy, int duration) {
            super.startScroll(startX, startY, dx, dy, 350);
        }
    }
}
